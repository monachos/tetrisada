# Tetrada

Tetris game written in Ada language for a text console.

## Requirements

To build the game Alire software is required. See https://alire.ada.dev/ for detailed information.

This project has been tested on Linux platform only.

## Building

``` bash
alr build
```

## Running

``` bash
alr run
```

## Game Rules

There are 4 difficulty levels to choose from, differing in the speed
at which the pieces (blocks) fall.
After selecting the difficulty level, a board appears on which pieces
of random colors and shapes fall from above.
The pieces fly at a constant speed and stop at the bottom edge of
the board or another piece (collision).
Pieces can be moved left and right using the arrow keys.
Pressing the down key speeds up the fall, and pressing the up key rotates
the piece around its axis by 90 degrees.
If the piece is on the left or right edge or next to another block and
rotation would cause a collision, rotation is not available.
Once an entire row is filled with chunks, that row is deleted.
If several lines are filled at the same time, individual lines are deleted
sequentially (with an animation effect).
Each row deletion increases the point counter by 1.
The game ends when the board is so full that it is not possible
to place a new block on the board.

## Navigation

At the bottom of the screen there is always a legend bar containing list of available keyboard shortcuts in a current context.

## Parential control

The game has a built-in parental control mechanism that limits the maximum
playing time to 30 minutes on a given day.
The time is counted regardless of whether the game is launched once
or several times a day.

## Technical information

This application uses Text User Interface (TUI) with standard color palete
of VT100 terminal.
In order to increase readability whole board (excluding legend bar on
the bottom of a screen)
one pixel is rendered by horizontally adjacent characters.
As a result, each pixel resembles is shaped like a square instead
of a vertical rectangle.
The game does not use a standard loop handling the sequence of keyboard reading,
model updating and rendering.
Instead, each of the game elements (rendering, keyboard operation, model change)
was run in a separate Ada task, working similarly to threads.
Individual tasks were synchronized via a protected object (`State_Type`)
using a rendezvous mechanism.
