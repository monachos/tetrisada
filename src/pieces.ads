with Canvases;
with Positive_Dimension;

--  Pieces management
package Pieces is
   Piece_Canvas_Size : Positive_Dimension.Dimension := (4, 4);

   function Create_Random_Piece return Canvases.Rectangle_Object;
   procedure Move_Left (Piece : in out Canvases.Rectangle_Object);
   procedure Move_Right (Piece : in out Canvases.Rectangle_Object);
   procedure Move_Down (Piece : in out Canvases.Rectangle_Object);
   procedure Rotate_Left (Piece : in out Canvases.Rectangle_Object);
   function Remove_Completed_Line (Canv : in out Canvases.Canvas_Type)
      return Boolean;
   function Is_Over_Field (
      Piece : Canvases.Rectangle_Object;
      Field : Canvases.Rectangle_Object)
         return Boolean;

private
   procedure Clear (Piece : in out Canvases.Rectangle_Object);
   procedure Set_I (Piece : in out Canvases.Rectangle_Object);
   procedure Set_L1 (Piece : in out Canvases.Rectangle_Object);
   procedure Set_L2 (Piece : in out Canvases.Rectangle_Object);
   procedure Set_S1 (Piece : in out Canvases.Rectangle_Object);
   procedure Set_S2 (Piece : in out Canvases.Rectangle_Object);
   procedure Set_T (Piece : in out Canvases.Rectangle_Object);
   procedure Set_O (Piece : in out Canvases.Rectangle_Object);
end Pieces;