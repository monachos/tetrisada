with Ada.Exceptions;
with Config;
with Integer_Coordinates;
with Keyboard;
with Logger;
with Manager;
with Screen_Playing;
with Terminal;
with Typography;

package body Screen_Final is
   Screen_Name : constant String := "Final screen";

   procedure Run (State : in out Game.State_Type) is
      Termination : Manager.Termination_Type;

      task Server_Task is
         entry Update_Screen;
         entry Quit;
      end Server_Task;

      task Screen_Refresh_Task;
      task Keyboard_Task;

      task body Server_Task is
      begin
         Logger.Debug (Screen_Name & ": Server Task started");
         Terminal.Erase_Screen;
         State.Show_Field;
         while not Termination.Is_Task_Terminated loop
            select
               accept Update_Screen do
                  Manager.Print_Changed_Screen (State);
               end Update_Screen;
            or
               accept Quit do
                  Termination.Terminate_Task;
                  State.Reset_Game;
                  State.Set_App_State (Game.State_Intro);
               end Quit;
            end select;
         end loop;
         Logger.Debug (Screen_Name & ": Server Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in screen task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Termination.Terminate_Task;
      end Server_Task;

      task body Screen_Refresh_Task is
      begin
         Logger.Debug (Screen_Name & ": Screen Refresh Task started");
         while not Termination.Is_Task_Terminated loop
            Server_Task.Update_Screen;
            delay Config.Screen_Refresh_Interval;
         end loop;
         Logger.Debug (Screen_Name & ": Screen Refresh Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in Screen Refresh task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Server_Task.Quit;
      end Screen_Refresh_Task;

      task body Keyboard_Task is
         Key : Keyboard.Keyboard_Code;
      begin
         Logger.Debug (Screen_Name & ": Keyboard Task started");
         while not Termination.Is_Task_Terminated loop
            Key := Keyboard.Read_Keyboard;
            case Key is
               when Keyboard.Key_Q => Server_Task.Quit;
               when others => null;
            end case;
         end loop;
         Logger.Debug (Screen_Name & ": Keyboard Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in keyboard task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Server_Task.Quit;
      end Keyboard_Task;
   begin
      null;
   end Run;

   procedure Paste_Screen (
      Canv : in out Canvases.Canvas_Type;
      App_State : Game.App_State_Type;
      Piece : Canvases.Rectangle_Object;
      Field : Canvases.Rectangle_Object;
      Score : Integer)
   is
   begin
      Screen_Playing.Paste_Screen (Canv, Piece, Field, Score);
      case App_State is
         when Game.State_Game_Over =>
            Paste_Information (Canv, "Game", "over");
         when Game.State_Timeout =>
            Paste_Information (Canv, "Time", "exceeded");
         when others => raise Game.Invalid_State_Exception;
      end case;
   end Paste_Screen;

   procedure Paste_Information (
      Canv : in out Canvases.Canvas_Type;
      Text1 : String;
      Text2 : String)
   is
      Game_Text : constant Canvases.Rectangle_Object
         := Typography.Create_Text (
            Text1,
            Config.Final_Text_Color);
      Game_Obj : constant Canvases.Rectangle_Object
         := Typography.Apply_Border (
            Game_Text,
            Config.Final_Background_Color);
      Game_Pos : constant Integer_Coordinates.Coordinates := (
         (Config.Screen_Size.Width - Game_Obj.Width) / 2,
         Config.Screen_Size.Height
         - 1
         - Game_Obj.Height);
      Over_Text : constant Canvases.Rectangle_Object
         := Typography.Create_Text (
            Text2,
            Config.Final_Text_Color);
      Over_Obj : constant Canvases.Rectangle_Object
         := Typography.Apply_Border (
            Over_Text,
            Config.Final_Background_Color);
      Over_Pos : constant Integer_Coordinates.Coordinates := (
         (Config.Screen_Size.Width - Over_Obj.Width) / 2,
         Game_Pos.Y - Game_Obj.Height + 1);
   begin
      Canvases.Paste (Canv, Game_Obj, Game_Pos);
      Canvases.Paste (Canv, Over_Obj, Over_Pos);
   end Paste_Information;

   function Get_Legend return Shortcut_Legend.Legend_Item_Vector.Vector
   is
      Legend : Shortcut_Legend.Legend_Item_Vector.Vector;
   begin
      Shortcut_Legend.Add_Item (Legend, "Q", "back to the home screen");
      return Legend;
   end Get_Legend;

end Screen_Final;