generic
   type Element_Type is private;

package Generic_Dimension is
   type Dimension is record
      Width, Height : Element_Type;
   end record;
end Generic_Dimension;
