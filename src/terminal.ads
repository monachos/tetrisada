--  VT100 terminal
--  https://www2.ccs.neu.edu/research/gpc/VonaUtils/vona/terminal/vtansi.htm
package Terminal is
   Escape : constant Character := Character'Val (8#33#);
   Attribute_Reset : constant String := "0";
   Attribute_Bright : constant String := "1";
   Attribute_Dim : constant String := "2";
   Attribute_Underscore : constant String := "4";
   Attribute_Blink : constant String := "5";
   Attribute_Reverse : constant String := "7";
   Attribute_Hidden : constant String := "8";
   Foreground_Black : constant String := "30";
   Foreground_Red : constant String := "31";
   Foreground_Green : constant String := "32";
   Foreground_Yellow : constant String := "33";
   Foreground_Blue : constant String := "34";
   Foreground_Magenta : constant String := "35";
   Foreground_Cyan : constant String := "36";
   Foreground_White : constant String := "37";
   Background_Black : constant String := "40";
   Background_Red : constant String := "41";
   Background_Green : constant String := "42";
   Background_Yellow : constant String := "43";
   Background_Blue : constant String := "44";
   Background_Magenta : constant String := "45";
   Background_Cyan : constant String := "46";
   Background_White : constant String := "47";

   Invalid_Color_Exception : exception;

   type Color is (
      Default,
      Transparent,
      Black,
      DarkGray,
      Red,
      LightRed,
      Green,
      LightGreen,
      Yellow,
      LightYellow,
      Blue,
      LightBlue,
      Magenta,
      LightMagenta,
      Cyan,
      LightCyan,
      LightGray,
      White
   );

   procedure Set_Text_Color (C : Color);
   procedure Set_Background_Color (C : Color);
   procedure Set_Cursor_Home;
   procedure Erase_Screen;
   procedure New_Line;
   procedure Put (Ch : Character);
   procedure Put (Text : String);

private
   function Get_Foreground_Color_Code (C : Color) return String;
   function Get_Background_Color_Code (C : Color) return String;
   procedure Set_Display_Attribute (Attr : String);
   procedure Set_Display_Attributes (
      Attr1 : String;
      Attr2 : String);
   procedure Set_Display_Attributes (
      Attr1 : String;
      Attr2 : String;
      Attr3 : String);
end Terminal;