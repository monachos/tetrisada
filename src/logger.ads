package Logger is
   Invalid_Logger_Level : exception;
   type Level is (
      Level_Debug,
      Level_Info,
      Level_Warning,
      Level_Error);

   procedure Init_Logger_File (File_Path : String);
   procedure Close_Logger_File;
   procedure Message (Lev : Level; Msg : String);
   procedure Debug (Msg : String);
   procedure Info (Msg : String);
   procedure Warning (Msg : String);
   procedure Error (Msg : String);

private
   function Format_Level_Text (Lev : Level)
      return String;
end Logger;