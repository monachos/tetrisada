with Integer_Coordinates;
with Shortcut_Legend;
with Terminal;

--  Canvases management
--  A canvas consists of a two-dimensional array of cells.
package Canvases is
   type Cell is record
      Visible : Boolean := False;
      Material : Boolean := False;
      Text : Character := ' ';
      Color : Terminal.Color := Terminal.Default;
      Background_Color : Terminal.Color := Terminal.Default;
   end record;

   type Canvas_Type is array (Positive range <>, Positive range <>) of Cell;

   type Rectangle_Object (Width : Positive; Height : Positive) is record
      Visible : Boolean := False;
      X : Integer := 1;
      Y : Integer := 1;
      Canvas : Canvas_Type (1 .. Width, 1 .. Height);
   end record;

   procedure Set_Background (
      Canv : in out Canvases.Canvas_Type;
      Background_Color : Terminal.Color);

   procedure Clear (Canv : in out Canvas_Type);
   function Equals (C1, C2 : Canvas_Type) return Boolean;
   function Is_Line_Completed (Canv : Canvas_Type; Y : Positive)
      return Boolean;
   procedure Clear_Line (Canv : in out Canvas_Type; Y : Positive);
   procedure Move_Range_Down (Canv : in out Canvas_Type;
      From_Line : Positive;
      To_Line : Positive);
   function Create_Filled_Rectangle (
      Width, Height : Positive;
      Fill_Color : Terminal.Color)
      return Rectangle_Object;
   procedure Paste (Tgt : in out Rectangle_Object; Src : Rectangle_Object);
   procedure Paste (
      Tgt : in out Canvas_Type;
      Src : Rectangle_Object;
      Offset : Integer_Coordinates.Coordinates);
   procedure Print (
      Canv : Canvas_Type;
      Legend : Shortcut_Legend.Legend_Item_Vector.Vector);

   function Is_In (X, Y : Integer;
      Obj : Rectangle_Object;
      Obj_Offset : Integer_Coordinates.Coordinates)
      return Boolean;
   procedure Hide_Cells_Above (Obj : in out Rectangle_Object; Ref_Y : Integer);

private
   procedure Print_Cell (C : Cell);

end Canvases;