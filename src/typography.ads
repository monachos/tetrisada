with Canvases;
with Terminal;

--  Text renderer
package Typography is
   Character_Height : constant Positive := 4;

   function Create_Text (Text : String; Text_Color : Terminal.Color)
      return Canvases.Rectangle_Object;
   function Apply_Border (
      Obj : Canvases.Rectangle_Object;
      Border_Color : Terminal.Color)
      return Canvases.Rectangle_Object;

private
   type Character_Definition_Type is
      array (Positive range <>, Positive range <>)
      of Boolean;

   function Create_Character (C : Wide_Character; Text_Color : Terminal.Color)
      return Canvases.Rectangle_Object;
   function Calculate_Character_Definition (C : Wide_Character)
      return Character_Definition_Type;
   procedure Apply_Border_Cell (
      Output_Canv : in out Canvases.Canvas_Type;
      Input_Canv : Canvases.Canvas_Type;
      X, Y : Integer;
      Border_Width : Integer;
      Border_Color : Terminal.Color);

end Typography;