with Ada.Containers.Ordered_Sets;
with Integer_Coordinates; use Integer_Coordinates;

package body Collider is
   package Coordinate_Set is new Ada.Containers.Ordered_Sets (
      Integer_Coordinates.Coordinates);

   function Collision_Detected (
      Bullet : Canvases.Rectangle_Object;
      Wall : Canvases.Rectangle_Object
      ) return Boolean
   is
      Coords : Coordinate_Set.Set;
   begin
      if not Wall.Visible then
         return False;
      end if;
      if not Bullet.Visible then
         return False;
      end if;

      --  Points inside the Wall object
      for X in Wall.Canvas'Range (1) loop
         for Y in Wall.Canvas'Range (2) loop
            if Wall.Canvas (X, Y).Material then
               declare
                  Co : Integer_Coordinates.Coordinates;
               begin
                  Co.X := Wall.X + X - 1;
                  Co.Y := Wall.Y + Y - 1;
                  Coords.Insert (Co);
               end;
            end if;
         end loop;
      end loop;

      --  Points on the left outside the Wall object
      for Y in Wall.Canvas'First (2) .. Wall.Canvas'Last (2) + 5 loop
         declare
            C : Integer_Coordinates.Coordinates;
         begin
            C.X := Wall.X - 1;
            C.Y := Wall.Y + Y - 1;
            Coords.Insert (C);
         end;
      end loop;

      --  Points on the right outside the Wall object
      for Y in Wall.Canvas'First (2) .. Wall.Canvas'Last (2) + 5 loop
         declare
            C : Integer_Coordinates.Coordinates;
         begin
            C.X := Wall.X + Wall.Canvas'Last (1);
            C.Y := Wall.Y + Y - 1;
            Coords.Insert (C);
         end;
      end loop;

      --  Points on the bottom side outside the Wall object
      for X in Wall.Canvas'Range (1) loop
         declare
            C : Integer_Coordinates.Coordinates;
         begin
            C.X := Wall.X + X - 1;
            C.Y := Wall.Y - 1;
            Coords.Insert (C);
         end;
      end loop;

      for X in Bullet.Canvas'Range (1) loop
         for Y in Bullet.Canvas'Range (2) loop
            if Bullet.Canvas (X, Y).Material then
               declare
                  Co : Integer_Coordinates.Coordinates;
               begin
                  Co.X := Bullet.X + X - 1;
                  Co.Y := Bullet.Y + Y - 1;
                  if Coordinate_Set.Contains (Coords, Co) then
                     return True;
                  end if;
               end;
            end if;
         end loop;
      end loop;

      return False;
   end Collision_Detected;

end Collider;