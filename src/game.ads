with Canvases;
with Config;
with Pieces;

--  Definitions for the game state machine.
package Game is
   Invalid_State_Exception : exception;

   type App_State_Type is (
      State_Intro,
      State_Playing,
      State_Game_Over,
      State_Timeout,
      State_Quit
   );

   subtype Level_Type is Positive range 1 .. 4;

   protected type State_Type is
      function Get_App_State return App_State_Type;
      procedure Set_App_State (St : App_State_Type);
      function Get_Level return Level_Type;
      procedure Set_Next_Level;
      procedure Set_Previous_Level;
      procedure Set_Canvas (Canv : Canvases.Canvas_Type; Result : out Boolean);
      procedure Reset_Game;
      function Get_Score return Integer;
      procedure Increment_Score;
      function Get_Piece return Canvases.Rectangle_Object;
      procedure Set_Piece (P : Canvases.Rectangle_Object);
      function Get_Field return Canvases.Rectangle_Object;
      procedure Show_Field;
      procedure Paste_Piece_Field;
      procedure Remove_Completed_Line_From_Field (Removed : out Boolean);

   private
      App_State : App_State_Type := State_Intro;
      Level : Level_Type := 1;
      Piece : Canvases.Rectangle_Object (
         Pieces.Piece_Canvas_Size.Width,
         Pieces.Piece_Canvas_Size.Height);
      Field : Canvases.Rectangle_Object (
         Config.Playing_Field_Size.Width,
         Config.Playing_Field_Size.Height);
      Current_Canvas : Canvases.Canvas_Type
         (1 .. Config.Screen_Size.Width, 1 .. Config.Screen_Size.Height);
      Score : Integer := 0;
   end State_Type;
end Game;