with Ada.Calendar;
with Ada.Directories;
with Ada.Environment_Variables;
with Ada.Strings.Fixed;

package body Config is
   function Get_App_Profile_Path return String
   is
      Home_Path : constant String
         := Ada.Environment_Variables.Value ("HOME");
   begin
      return Home_Path & "/.tetris";
   end Get_App_Profile_Path;

   procedure Create_Profile_Directory_Of_Does_Not_Exist
   is
      Profile_Path : constant String := Get_App_Profile_Path;
   begin
      if not Ada.Directories.Exists (Profile_Path) then
         Ada.Directories.Create_Path (Profile_Path);
      end if;
   end Create_Profile_Directory_Of_Does_Not_Exist;

   function Get_File_Base_Name return String
   is
      Now : constant Ada.Calendar.Time := Ada.Calendar.Clock;
      Year_Text : constant String := Ada.Strings.Fixed.Trim (
         Ada.Calendar.Year (Now)'Image,
         Ada.Strings.Left);
      Month_Text : constant String := Ada.Strings.Fixed.Trim (
         Ada.Calendar.Month (Now)'Image,
         Ada.Strings.Left);
      Day_Text : constant String := Ada.Strings.Fixed.Trim (
         Ada.Calendar.Day (Now)'Image,
         Ada.Strings.Left);
      File_Base : constant String
         := Year_Text & "-" & Month_Text & "-" & Day_Text;
   begin
      return File_Base;
   end Get_File_Base_Name;

   function Get_Log_File_Path return String
   is
      File_Name : constant String
         := Config.Get_File_Base_Name & ".log";
      Profile_Path : constant String := Config.Get_App_Profile_Path;
   begin
      return Profile_Path & "/" & File_Name;
   end Get_Log_File_Path;
end Config;