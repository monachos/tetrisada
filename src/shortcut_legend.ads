with Ada.Containers.Indefinite_Vectors;

package Shortcut_Legend is
   type Legend_Item (Key_Length, Desc_Length : Positive) is record
      Key : String (1 .. Key_Length);
      Description : String (1 .. Desc_Length);
   end record;

   package Legend_Item_Vector is
      new Ada.Containers.Indefinite_Vectors (
         Index_Type   => Positive,
         Element_Type => Legend_Item
      );

   procedure Add_Item (
      Container : in out Legend_Item_Vector.Vector;
      Key : String;
      Description : String);

end Shortcut_Legend;