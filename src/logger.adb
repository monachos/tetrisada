with Ada.Text_IO;

package body Logger is
   package TIO renames Ada.Text_IO;

   Log_File : TIO.File_Type;

   procedure Init_Logger_File (File_Path : String)
   is
   begin
      TIO.Open (
         File => Log_File,
         Mode => TIO.Append_File,
         Name => File_Path);
   exception
      when TIO.Name_Error =>
         TIO.Create (
            File => Log_File,
            Mode => TIO.Append_File,
            Name => File_Path);
   end Init_Logger_File;

   procedure Close_Logger_File is
   begin
      TIO.Close (Log_File);
   end Close_Logger_File;

   procedure Message (Lev : Level; Msg : String) is
   begin
      TIO.Put_Line (
         Log_File,
         Format_Level_Text (Lev) & ": " & Msg);
   end Message;

   procedure Debug (Msg : String) is
   begin
      Message (Level_Debug, Msg);
   end Debug;

   procedure Info (Msg : String) is
   begin
      Message (Level_Info, Msg);
   end Info;

   procedure Warning (Msg : String) is
   begin
      Message (Level_Warning, Msg);
   end Warning;

   procedure Error (Msg : String) is
   begin
      Message (Level_Error, Msg);
   end Error;

   function Format_Level_Text (Lev : Level)
      return String is
   begin
      case Lev is
         when Level_Debug => return "DEB";
         when Level_Info => return "INF";
         when Level_Warning => return "WRN";
         when Level_Error => return "ERR";
      end case;
   end Format_Level_Text;

end Logger;