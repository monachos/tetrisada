with Canvases;
with Game;
with Shortcut_Legend;

package Screen_Final is
   procedure Run (State : in out Game.State_Type);

   procedure Paste_Screen (
      Canv : in out Canvases.Canvas_Type;
      App_State : Game.App_State_Type;
      Piece : Canvases.Rectangle_Object;
      Field : Canvases.Rectangle_Object;
      Score : Integer);

   function Get_Legend return Shortcut_Legend.Legend_Item_Vector.Vector;

private
   procedure Paste_Information (
      Canv : in out Canvases.Canvas_Type;
      Text1 : String;
      Text2 : String);
end Screen_Final;