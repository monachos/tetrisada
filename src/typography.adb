with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Strings.UTF_Encoding; use Ada.Strings.UTF_Encoding;
with Ada.Strings.UTF_Encoding.Conversions;
with Integer_Coordinates;

package body Typography is
   function Create_Text (Text : String; Text_Color : Terminal.Color)
      return Canvases.Rectangle_Object
   is
      Total_Width, Total_Height : Integer := 0;
      Space_Width : constant Positive := 1;
      Wide_Text : constant Wide_String
         := Conversions.Convert (UTF_8_String (Text));
   begin
      for I in 1 .. Wide_Text'Length loop
         declare
            C : constant Wide_Character := Wide_Text (I);
            Char_Obj : constant Canvases.Rectangle_Object
               := Create_Character (C, Text_Color);
         begin
            Total_Width := Total_Width + Char_Obj.Width;
            if Char_Obj.Height > Total_Height then
               Total_Height := Char_Obj.Height;
            end if;
         end;
      end loop;
      Total_Width := Total_Width + (Text'Length - 1) * Space_Width;
      declare
         Obj : Canvases.Rectangle_Object (Total_Width, Total_Height);
         Pos : Integer_Coordinates.Coordinates := (0, 0);
      begin
         Obj.Visible := True;
         for I in 1 .. Wide_Text'Length loop
            declare
               C : constant Wide_Character := Wide_Text (I);
               Char_Obj : constant Canvases.Rectangle_Object
                  := Create_Character (C, Text_Color);
            begin
               Canvases.Paste (Obj.Canvas, Char_Obj, Pos);
               Pos.X := Pos.X + Char_Obj.Width + Space_Width;
            end;
         end loop;
         return Obj;
      end;
   end Create_Text;

   function Get_Raw_Character_Stream (C : Wide_Character)
      return String
   is
   begin
      return (case C is
         when 'a' =>
            "    " & ASCII.LF &
            " xx " & ASCII.LF &
            "x x " & ASCII.LF &
            "x x " & ASCII.LF &
            " xxx" & ASCII.LF &
            "    ",
         when 'c' =>
            "   " & ASCII.LF &
            " xx" & ASCII.LF &
            "x  " & ASCII.LF &
            "x  " & ASCII.LF &
            " xx" & ASCII.LF &
            "   ",
         when 'C' =>
            " xx" & ASCII.LF &
            "x  " & ASCII.LF &
            "x  " & ASCII.LF &
            "x  " & ASCII.LF &
            " xx" & ASCII.LF &
            "   ",
         when 'd' =>
            "  x" & ASCII.LF &
            " xx" & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            " xx" & ASCII.LF &
            "   ",
         when 'e' =>
            "   " & ASCII.LF &
            " x " & ASCII.LF &
            "x x" & ASCII.LF &
            "x  " & ASCII.LF &
            " xx" & ASCII.LF &
            "   ",
         when 'g' =>
            "   " & ASCII.LF &
            " x " & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            " xx" & ASCII.LF &
            "x  ",
         when 'G' =>
            " xx " & ASCII.LF &
            "x   " & ASCII.LF &
            "x xx" & ASCII.LF &
            "x  x" & ASCII.LF &
            " xx " & ASCII.LF &
            "    ",
         when 'i' =>
            "x" & ASCII.LF &
            " " & ASCII.LF &
            "x" & ASCII.LF &
            "x" & ASCII.LF &
            "x" & ASCII.LF &
            " ",
         when 'K' =>
            "x  x" & ASCII.LF &
            "x x " & ASCII.LF &
            "xx  " & ASCII.LF &
            "x x " & ASCII.LF &
            "x  x" & ASCII.LF &
            "    ",
         when 'm' =>
            "     " & ASCII.LF &
            " xxxx" & ASCII.LF &
            "x x x" & ASCII.LF &
            "x x x" & ASCII.LF &
            "x x x" & ASCII.LF &
            "     ",
         when 'n' =>
            "   " & ASCII.LF &
            " xx" & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            "   ",
         when 'P' =>
            "xxx " & ASCII.LF &
            "x  x" & ASCII.LF &
            "xxx " & ASCII.LF &
            "x   " & ASCII.LF &
            "x   " & ASCII.LF &
            "    ",
         when 'o' =>
            "    " & ASCII.LF &
            " xx " & ASCII.LF &
            "x  x" & ASCII.LF &
            "x  x" & ASCII.LF &
            " xx " & ASCII.LF &
            "    ",
         when 'r' =>
            "  " & ASCII.LF &
            "  " & ASCII.LF &
            "xx" & ASCII.LF &
            "x " & ASCII.LF &
            "x " & ASCII.LF &
            "  ",
         when 's' =>
            "   " & ASCII.LF &
            " xx" & ASCII.LF &
            "x  " & ASCII.LF &
            "  x" & ASCII.LF &
            "xx " & ASCII.LF &
            "   ",
         when 't' =>
            " x " & ASCII.LF &
            "xxx" & ASCII.LF &
            " x " & ASCII.LF &
            " x " & ASCII.LF &
            " xx" & ASCII.LF &
            "   ",
         when 'T' =>
            "xxxxx" & ASCII.LF &
            "  x  " & ASCII.LF &
            "  x  " & ASCII.LF &
            "  x  " & ASCII.LF &
            "  x  " & ASCII.LF &
            "     ",
         when 'v' =>
            "   " & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            " x " & ASCII.LF &
            "   ",
         when 'x' =>
            "   " & ASCII.LF &
            "   " & ASCII.LF &
            "x x" & ASCII.LF &
            " x " & ASCII.LF &
            "x x" & ASCII.LF &
            "   ",
         when 'y' =>
            "   " & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            " x " & ASCII.LF &
            "x  ",
         when 'z' =>
            "   " & ASCII.LF &
            "xxx" & ASCII.LF &
            "  x" & ASCII.LF &
            " x " & ASCII.LF &
            "xxx" & ASCII.LF &
            "   ",
         when '0' =>
            " x " & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            "x x" & ASCII.LF &
            " x " & ASCII.LF &
            "   ",
         when '1' =>
            " x" & ASCII.LF &
            "xx" & ASCII.LF &
            " x" & ASCII.LF &
            " x" & ASCII.LF &
            " x" & ASCII.LF &
            "  ",
         when '2' =>
            " xx " & ASCII.LF &
            "x  x" & ASCII.LF &
            "  x " & ASCII.LF &
            " x  " & ASCII.LF &
            "xxxx" & ASCII.LF &
            "    ",
         when '3' =>
            " xx " & ASCII.LF &
            "x  x" & ASCII.LF &
            "  x " & ASCII.LF &
            "x  x" & ASCII.LF &
            " xx " & ASCII.LF &
            "    ",
         when '4' =>
            " xx " & ASCII.LF &
            "x x " & ASCII.LF &
            "xxxx" & ASCII.LF &
            "  x " & ASCII.LF &
            "  x " & ASCII.LF &
            "    ",
         when '5' =>
            "xxx" & ASCII.LF &
            "x  " & ASCII.LF &
            "xx " & ASCII.LF &
            "  x" & ASCII.LF &
            "xx " & ASCII.LF &
            "   ",
         when '6' =>
            " xx " & ASCII.LF &
            "x   " & ASCII.LF &
            "xxx " & ASCII.LF &
            "x  x" & ASCII.LF &
            " xx " & ASCII.LF &
            "    ",
         when '7' =>
            "xxx" & ASCII.LF &
            "  x" & ASCII.LF &
            "  x" & ASCII.LF &
            " x " & ASCII.LF &
            "x  " & ASCII.LF &
            "   ",
         when '8' =>
            " xx " & ASCII.LF &
            "x  x" & ASCII.LF &
            " xx " & ASCII.LF &
            "x  x" & ASCII.LF &
            " xx " & ASCII.LF &
            "    ",
         when '9' =>
            " xx " & ASCII.LF &
            "x  x" & ASCII.LF &
            " xxx" & ASCII.LF &
            "   x" & ASCII.LF &
            " xx " & ASCII.LF &
            "    ",
         when ' ' =>
            " " & ASCII.LF &
            " " & ASCII.LF &
            " " & ASCII.LF &
            " " & ASCII.LF &
            " " & ASCII.LF &
            " ",
         when ':' =>
            " " & ASCII.LF &
            "x" & ASCII.LF &
            " " & ASCII.LF &
            "x" & ASCII.LF &
            " " & ASCII.LF &
            " ",
         when others =>
            "xxx" & ASCII.LF &
            "xxx" & ASCII.LF &
            "xxx" & ASCII.LF &
            "xxx" & ASCII.LF &
            "xxx" & ASCII.LF &
            "   "
      );
   end Get_Raw_Character_Stream;

   package Character_Definition_Map_Type is new
     Ada.Containers.Indefinite_Ordered_Maps (
         Key_Type => Wide_Character,
         Element_Type => Character_Definition_Type
      );

   Character_Definition_Cache : Character_Definition_Map_Type.Map;

   function Get_Character_Definition (C : Wide_Character)
      return Character_Definition_Type
   is
   begin
      if Character_Definition_Cache.Contains (C) then
         return Character_Definition_Cache (C);
      end if;
      declare
         Def : constant Character_Definition_Type
            := Calculate_Character_Definition (C);
      begin
         Character_Definition_Cache.Include (C, Def);
         return Def;
      end;
   end Get_Character_Definition;

   function Calculate_Character_Definition (C : Wide_Character)
      return Character_Definition_Type
   is
      Raw : constant String := Get_Raw_Character_Stream (C);
      X, Y : Integer := 0;
      Width, Height : Integer := 0;
   begin
      --  calculate dimensions
      X := 0;
      Y := 1;
      for I in 1 .. Raw'Length loop
         case Raw (I) is
            when ASCII.LF =>
               if Width < X then
                  Width := X;
               end if;
               X := 0;
               Y := Y + 1;
            when others =>
               X := X + 1;
         end case;
      end loop;
      if Width < X then
         Width := X;
      end if;
      Height := Y;

      declare
         Def : Character_Definition_Type (1 .. Width, 1 .. Height);
      begin
         --  init array
         for Def_Y in Def'Range (2) loop
            for Def_X in Def'Range (1) loop
               Def (Def_X, Def_Y) := False;
            end loop;
         end loop;
         X := 1;
         Y := Height;
         for I in 1 .. Raw'Length loop
            case Raw (I) is
               when ASCII.LF =>
                  X := 1;
                  Y := Y - 1;
               when ' ' =>
                  X := X + 1;
               when others =>
                  Def (X, Y) := True;
                  X := X + 1;
            end case;
         end loop;
         return Def;
      end;
   end Calculate_Character_Definition;

   function Create_Character (C : Wide_Character; Text_Color : Terminal.Color)
      return Canvases.Rectangle_Object
   is
      Def : constant Character_Definition_Type
         := Get_Character_Definition (C);
      Obj : Canvases.Rectangle_Object (Def'Last (1), Def'Last (2));
   begin
      Obj.Visible := True;
      Canvases.Clear (Obj.Canvas);
      for Y in Def'Range (2) loop
         for X in Def'Range (1) loop
            if Def (X, Y) then
               declare
                  C : Canvases.Cell;
               begin
                  C.Visible := True;
                  C.Background_Color := Text_Color;
                  Obj.Canvas (X, Y) := C;
               end;
            end if;
         end loop;
      end loop;
      return Obj;
   end Create_Character;

   function Apply_Border (
      Obj : Canvases.Rectangle_Object;
      Border_Color : Terminal.Color)
      return Canvases.Rectangle_Object
   is
      Border_Width : constant Integer := 1;
      Ext_Obj : Canvases.Rectangle_Object (
         Obj.Width + 2 * Border_Width,
         Obj.Height + 2 * Border_Width);
      Border_Obj : Canvases.Rectangle_Object (
         Obj.Width + 2 * Border_Width,
         Obj.Height + 2 * Border_Width);
      Pos : constant Integer_Coordinates.Coordinates := (0, 0);
   begin
      Ext_Obj.Visible := Obj.Visible;
      Ext_Obj.X := Obj.X;
      Ext_Obj.Y := Obj.Y;
      Border_Obj.Visible := True;
      Canvases.Clear (Ext_Obj.Canvas);
      for Y in Obj.Canvas'Range (2) loop
         for X in Obj.Canvas'Range (1) loop
            Ext_Obj.Canvas (X + 1, Y + 1) := Obj.Canvas (X, Y);
         end loop;
      end loop;
      for Y in Ext_Obj.Canvas'Range (2) loop
         for X in Ext_Obj.Canvas'Range (1) loop
            if Ext_Obj.Canvas (X, Y).Visible then
               Apply_Border_Cell (
                  Border_Obj.Canvas,
                  Ext_Obj.Canvas,
                  X, Y,
                  Border_Width,
                  Border_Color);
            end if;
         end loop;
      end loop;
      Canvases.Paste (Ext_Obj.Canvas, Border_Obj, Pos);
      return Ext_Obj;
   end Apply_Border;

   procedure Apply_Border_Cell (
      Output_Canv : in out Canvases.Canvas_Type;
      Input_Canv : Canvases.Canvas_Type;
      X, Y : Integer;
      Border_Width : Integer;
      Border_Color : Terminal.Color)
   is
   begin
      for Offset_Y in -Border_Width .. Border_Width loop
         for Offset_X in -Border_Width .. Border_Width loop
            if Offset_X /= 0 or else Offset_Y /= 0 then
               declare
                  Cur_X : constant Integer := X + Offset_X;
                  Cur_Y : constant Integer := Y + Offset_Y;
               begin
                  if Cur_X in Input_Canv'Range (1)
                     and then Cur_Y in Input_Canv'Range (2)
                  then
                     if not Input_Canv (Cur_X, Cur_Y).Visible then
                        declare
                           C : Canvases.Cell;
                        begin
                           C.Visible := True;
                           C.Material := Input_Canv (X, Y).Material;
                           C.Background_Color := Border_Color;
                           Output_Canv (Cur_X, Cur_Y) := C;
                        end;
                     end if;
                  end if;
               end;
            end if;
         end loop;
      end loop;
   end Apply_Border_Cell;

end Typography;