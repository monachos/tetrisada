with Ada.Exceptions;
with Ada.Strings.Fixed;
with Config;
with Integer_Coordinates;
with Keyboard;
with Logger;
with Manager;
with Terminal;
with Typography;

package body Screen_Intro is
   Screen_Name : constant String := "Intro screen";

   procedure Run (State : in out Game.State_Type) is
      Termination : Manager.Termination_Type;

      task Server_Task is
         entry Update_Screen;
         entry Move_Left;
         entry Move_Right;
         entry Play;
         entry Quit;
      end Server_Task;

      task Screen_Refresh_Task;
      task Keyboard_Task;

      task body Server_Task is
      begin
         Logger.Debug (Screen_Name & ": Server Task started");
         Terminal.Erase_Screen;
         while not Termination.Is_Task_Terminated loop
            select
               accept Update_Screen do
                  Manager.Print_Changed_Screen (State);
               end Update_Screen;
            or
               accept Move_Left do
                  State.Set_Previous_Level;
               end Move_Left;
            or
               accept Move_Right do
                  State.Set_Next_Level;
               end Move_Right;
            or
               accept Play do
                  State.Set_App_State (Game.State_Playing);
                  State.Reset_Game;
                  Termination.Terminate_Task;
               end Play;
            or
               accept Quit do
                  State.Set_App_State (Game.State_Quit);
                  Termination.Terminate_Task;
               end Quit;
            end select;
         end loop;
         Logger.Debug (Screen_Name & ": Server Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in screen task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Termination.Terminate_Task;
      end Server_Task;

      task body Screen_Refresh_Task is
      begin
         Logger.Debug (Screen_Name & ": Screen Refresh Task started");
         while not Termination.Is_Task_Terminated loop
            Server_Task.Update_Screen;
            delay Config.Screen_Refresh_Interval;
         end loop;
         Logger.Debug (Screen_Name & ": Screen Refresh Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in Screen Refresh task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Server_Task.Quit;
      end Screen_Refresh_Task;

      task body Keyboard_Task is
         Key : Keyboard.Keyboard_Code;
      begin
         Logger.Debug (Screen_Name & ": Keyboard Task started");
         while not Termination.Is_Task_Terminated loop
            Key := Keyboard.Read_Keyboard;
            case Key is
               when Keyboard.Key_Left => Server_Task.Move_Left;
               when Keyboard.Key_Right => Server_Task.Move_Right;
               when Keyboard.Key_Enter => Server_Task.Play;
               when Keyboard.Key_Q => Server_Task.Quit;
               when others => null;
            end case;
         end loop;
         Logger.Debug (Screen_Name & ": Keyboard Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in keyboard task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Server_Task.Quit;
      end Keyboard_Task;
   begin
      null;
   end Run;

   procedure Paste_Screen (
      Canv : in out Canvases.Canvas_Type;
      Level : Game.Level_Type)
   is
   begin
      Canvases.Set_Background (Canv, Config.Screen_Color);
      Paste_Title (Canv);
      Paste_Levels (Canv, Level);
   end Paste_Screen;

   procedure Paste_Title (Canv : in out Canvases.Canvas_Type)
   is
      Text_Obj : constant Canvases.Rectangle_Object
         := Typography.Create_Text (Config.Game_Name, Config.Intro_Title_Color);
      Pos : constant Integer_Coordinates.Coordinates := (
         (Config.Screen_Size.Width - Text_Obj.Width) / 2,
         Config.Playing_Field_Pos.Y
            + Config.Playing_Field_Size.Height
            - Text_Obj.Height);
   begin
      Canvases.Paste (Canv, Text_Obj, Pos);
   end Paste_Title;

   procedure Paste_Levels (
      Canv : in out Canvases.Canvas_Type;
      Level : Game.Level_Type)
   is
      X_Step : constant Integer  := Config.Screen_Size.Width / 6;
      Pos : Integer_Coordinates.Coordinates := (
         X_Step - 2,
         Config.Playing_Field_Pos.Y);
      Max_Text_Height : Integer := 0;
   begin
      for L in Game.Level_Type'Range loop
         declare
            Level_Text : constant String := Ada.Strings.Fixed.Trim (
               L'Image,
               Ada.Strings.Left
            );
            Text_Obj : constant Canvases.Rectangle_Object
               := Typography.Create_Text (
                  Level_Text,
                  Config.Intro_Level_Color);
         begin
            if Max_Text_Height < Text_Obj.Height then
               Max_Text_Height := Text_Obj.Height;
            end if;
            if L = Level then
               declare
                  Rect_Obj : constant Canvases.Rectangle_Object
                     := Canvases.Create_Filled_Rectangle (
                        Text_Obj.Width + 2,
                        Text_Obj.Height + 2,
                        Config.Intro_Level_Selector_Color);
                  Rect_Pos : constant Integer_Coordinates.Coordinates := (
                     Pos.X - 1,
                     Pos.Y - 1);
               begin
                  Canvases.Paste (Canv, Rect_Obj, Rect_Pos);
               end;
            end if;
            Canvases.Paste (Canv, Text_Obj, Pos);
            Pos.X := Pos.X + Text_Obj.Width + X_Step;
         end;
      end loop;
   end Paste_Levels;

   function Get_Legend return Shortcut_Legend.Legend_Item_Vector.Vector
   is
      Legend : Shortcut_Legend.Legend_Item_Vector.Vector;
   begin
      Shortcut_Legend.Add_Item (Legend, "Q", "quit program");
      Shortcut_Legend.Add_Item (Legend, "left right", "level selection");
      Shortcut_Legend.Add_Item (Legend, "Enter", "play");
      return Legend;
   end Get_Legend;

end Screen_Intro;