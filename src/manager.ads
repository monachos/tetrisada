with Canvases;
with Game;
with Shortcut_Legend;

--  State machine of the game
package Manager is
   protected type Termination_Type is
      procedure Terminate_Task;
      function Is_Task_Terminated return Boolean;
   private
      Termination_Flag : Boolean := False;
   end Termination_Type;

   procedure Run;
   procedure Print_Changed_Screen (State : in out Game.State_Type);

private
   function Compose_Screen (State : in out Game.State_Type)
      return Canvases.Canvas_Type;
   function Get_Legend (App_State : Game.App_State_Type)
      return Shortcut_Legend.Legend_Item_Vector.Vector;
end Manager;