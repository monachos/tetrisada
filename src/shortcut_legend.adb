package body Shortcut_Legend is
   procedure Add_Item (
      Container : in out Legend_Item_Vector.Vector;
      Key : String;
      Description : String)
   is
      Item : constant Legend_Item := (
         Key_Length => Key'Length,
         Key => Key,
         Desc_Length => Description'Length,
         Description => Description
      );
   begin
      Container.Append (Item);
   end Add_Item;

end Shortcut_Legend;