package body Generic_Coordinates is
   function "<" (Left, Right : Coordinates) return Boolean
   is
   begin
      if Left.Y < Right.Y then
         return True;
      elsif Left.Y = Right.Y then
         return Left.X < Right.X;
      else
         return False;
      end if;
   end "<";

   overriding function "=" (Left, Right : Coordinates) return Boolean
   is
   begin
      return Left.X = Right.X and then Left.Y = Right.Y;
   end "=";
end Generic_Coordinates;