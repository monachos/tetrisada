with Canvases;

--  Collision detector
package Collider is
   function Collision_Detected (
      Bullet : Canvases.Rectangle_Object;
      Wall : Canvases.Rectangle_Object
      ) return Boolean;

end Collider;