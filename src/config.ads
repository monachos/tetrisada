with Integer_Coordinates;
with Positive_Dimension;
with Terminal;

--  Miscellaneous configuration data
package Config is
   Game_Name : constant String := "Tetrisada";
   Game_Name_Part_1 : constant String := "Tetris";
   Game_Name_Part_2 : constant String := "ada";
   Screen_Refresh_Interval : constant Duration := 0.02;
   Step_Interval_1 : constant Duration := 0.5;
   Step_Interval_2 : constant Duration := 0.3;
   Step_Interval_3 : constant Duration := 0.1;
   Step_Interval_4 : constant Duration := 0.05;
   Line_Removing_Interval : constant Duration := 0.2;
   Limit_Of_Minutes_Per_Day : constant Positive := 30;
   Parental_Check_Interval : constant Duration := 0.1;
   Parental_Check_Iterations : constant Positive := 100;

   Screen_Size : constant Positive_Dimension.Dimension := (40, 24);
   Screen_Color : Terminal.Color := Terminal.LightGray;

   --  Legend
   Legend_Background_Color : Terminal.Color := Terminal.Black;
   Legend_Key_Color : Terminal.Color := Terminal.LightRed;
   Legend_Description_Color : Terminal.Color := Terminal.White;

   --  Screen_Intro
   Intro_Title_Color : Terminal.Color := Terminal.Blue;
   Intro_Level_Color : Terminal.Color := Terminal.Green;
   Intro_Level_Selector_Color : Terminal.Color := Terminal.LightYellow;

   --  Screen_Playing
   Playing_Title_Offset : constant Positive := 2;
   Playing_Title_Color : Terminal.Color := Terminal.Blue;
   Playing_Field_Pos : constant Integer_Coordinates.Coordinates := (2, 2);
   Playing_Field_Size : constant Positive_Dimension.Dimension := (10, 20);
   Playing_Field_Color : Terminal.Color := Terminal.Black;
   Playing_Score_Offset : constant Positive := 2;
   Playing_Score_Label_Color : Terminal.Color := Terminal.Black;
   Playing_Score_Value_Color : Terminal.Color := Terminal.Green;

   --  Screen_Final
   Final_Text_Color : Terminal.Color := Terminal.Red;
   Final_Background_Color : Terminal.Color := Terminal.Yellow;

   function Get_App_Profile_Path return String;
   procedure Create_Profile_Directory_Of_Does_Not_Exist;
   function Get_File_Base_Name return String;
   function Get_Log_File_Path return String;
end Config;