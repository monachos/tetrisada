with Logger;
with Config;
with Screen_Final;
with Screen_Intro;
with Screen_Playing;

package body Manager is
   protected body Termination_Type is
      procedure Terminate_Task is
      begin
         Termination_Flag := True;
      end Terminate_Task;

      function Is_Task_Terminated return Boolean is
      begin
         return Termination_Flag;
      end Is_Task_Terminated;
   end Termination_Type;

   procedure Run is
      State : Game.State_Type;
   begin
      Logger.Info ("Game manager started");
      loop
         case State.Get_App_State is
            when Game.State_Intro => Screen_Intro.Run (State);
            when Game.State_Playing => Screen_Playing.Run (State);
            when Game.State_Game_Over | Game.State_Timeout =>
               Screen_Final.Run (State);
            when Game.State_Quit => exit;
         end case;
      end loop;
   end Run;

   procedure Print_Changed_Screen (State : in out Game.State_Type)
   is
      Canv : constant Canvases.Canvas_Type := Compose_Screen (State);
      Has_Been_Set : Boolean := False;
   begin
      State.Set_Canvas (Canv, Has_Been_Set);
      if Has_Been_Set then
         declare
            Legend : constant Shortcut_Legend.Legend_Item_Vector.Vector
               := Get_Legend (State.Get_App_State);
         begin
            Canvases.Print (Canv, Legend);
         end;
      end if;
   end Print_Changed_Screen;

   function Compose_Screen (State : in out Game.State_Type)
      return Canvases.Canvas_Type
   is
      Canv : Canvases.Canvas_Type
         (1 .. Config.Screen_Size.Width, 1 .. Config.Screen_Size.Height);
   begin
      case State.Get_App_State is
         when Game.State_Intro =>
            Screen_Intro.Paste_Screen (Canv, State.Get_Level);
         when Game.State_Playing =>
            Screen_Playing.Paste_Screen (
               Canv,
               State.Get_Piece,
               State.Get_Field,
               State.Get_Score
            );
         when Game.State_Game_Over | Game.State_Timeout =>
            Screen_Final.Paste_Screen (
               Canv,
               State.Get_App_State,
               State.Get_Piece,
               State.Get_Field,
               State.Get_Score
            );
         when Game.State_Quit => null;
      end case;
      return Canv;
   end Compose_Screen;

   function Get_Legend (App_State : Game.App_State_Type)
      return Shortcut_Legend.Legend_Item_Vector.Vector
   is
   begin
      return (case App_State is
         when Game.State_Intro => Screen_Intro.Get_Legend,
         when Game.State_Playing => Screen_Playing.Get_Legend,
         when Game.State_Game_Over | Game.State_Timeout =>
            Screen_Final.Get_Legend,
         when Game.State_Quit => raise Game.Invalid_State_Exception
      );
   end Get_Legend;

end Manager;