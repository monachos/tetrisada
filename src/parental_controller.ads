--  Parental controlling.
--  This package analyzes the log file to detect when the maximum gaming time
--  has been exceeded during the current day.
package Parental_Controller is
   procedure Init_Tick_Counter;
   function Time_Exceeded return Boolean;
   procedure Register_Tick;

private
   Tick_Marker_Name : constant String := "Hearbit notification";
   Tick_Counter : Integer := 0;
end Parental_Controller;