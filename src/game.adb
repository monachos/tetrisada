package body Game is
   protected body State_Type is
      function Get_App_State return App_State_Type
      is
      begin
         return App_State;
      end Get_App_State;

      procedure Set_App_State (St : App_State_Type)
      is
      begin
         App_State := St;
      end Set_App_State;

      function Get_Level return Level_Type
      is
      begin
         return Level;
      end Get_Level;

      procedure Set_Next_Level
      is
      begin
         if Level < Level_Type'Last then
            Level := Level + 1;
         end if;
      end Set_Next_Level;

      procedure Set_Previous_Level
      is
      begin
         if Level > Level_Type'First then
            Level := Level - 1;
         end if;
      end Set_Previous_Level;

      procedure Set_Canvas (Canv : Canvases.Canvas_Type; Result : out Boolean)
      is
         Eq : constant Boolean := Canvases.Equals (Canv, Current_Canvas);
      begin
         if not Eq then
            Current_Canvas := Canv;
            Result := True;
         else
            Result := False;
         end if;
      end Set_Canvas;

      procedure Reset_Game
      is
      begin
         Score := 0;
         Canvases.Clear (Field.Canvas);
      end Reset_Game;

      function Get_Score return Integer
      is
      begin
         return Score;
      end Get_Score;

      procedure Increment_Score
      is
      begin
         Score := Score + 1;
      end Increment_Score;

      function Get_Piece return Canvases.Rectangle_Object
      is
      begin
         return Piece;
      end Get_Piece;

      procedure Set_Piece (P : Canvases.Rectangle_Object)
      is
      begin
         Piece := P;
      end Set_Piece;

      function Get_Field return Canvases.Rectangle_Object
      is
      begin
         return Field;
      end Get_Field;

      procedure Show_Field
      is
      begin
         Field.Visible := True;
      end Show_Field;

      procedure Paste_Piece_Field
      is
      begin
         Canvases.Paste (Field, Piece);
      end Paste_Piece_Field;

      procedure Remove_Completed_Line_From_Field (Removed : out Boolean)
      is
      begin
         Removed := Pieces.Remove_Completed_Line (Field.Canvas);
      end Remove_Completed_Line_From_Field;

   end State_Type;

end Game;