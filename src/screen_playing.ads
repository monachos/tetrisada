with Canvases;
with Game;
with Manager;
with Shortcut_Legend;

package Screen_Playing is
   procedure Run (State : in out Game.State_Type);

   procedure Paste_Screen (
      Canv : in out Canvases.Canvas_Type;
      Piece : Canvases.Rectangle_Object;
      Field : Canvases.Rectangle_Object;
      Score : Integer);

   function Get_Legend return Shortcut_Legend.Legend_Item_Vector.Vector;

private
   procedure Move_Piece_Left (State : in out Game.State_Type);
   procedure Rotate_Piece (State : in out Game.State_Type);
   procedure Move_Piece_Right (State : in out Game.State_Type);
   procedure Move_Piece_Down (
      State : in out Game.State_Type;
      Termination : in out Manager.Termination_Type);
   procedure Paste_Game_Background (
      Canv : in out Canvases.Canvas_Type;
      Field : Canvases.Rectangle_Object);
   procedure Paste_Title (Canv : in out Canvases.Canvas_Type);
   procedure Paste_Score (Canv : in out Canvases.Canvas_Type; Score : Integer);
end Screen_Playing;