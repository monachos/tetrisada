generic
   type Element_Type is private;
   with function "<" (Left, Right : Element_Type) return Boolean is <>;
   with function "=" (Left, Right : Element_Type) return Boolean is <>;

package Generic_Coordinates is
   type Coordinates is record
      X, Y : Element_Type;
   end record;

   function "<" (Left, Right : Coordinates) return Boolean;
   overriding function "=" (Left, Right : Coordinates) return Boolean;
end Generic_Coordinates;
