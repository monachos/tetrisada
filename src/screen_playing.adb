with Ada.Exceptions;
with Ada.Strings.Fixed;
with Collider;
with Config;
with Integer_Coordinates;
with Keyboard;
with Logger;
with Parental_Controller;
with Pieces;
with Terminal;
with Typography;

package body Screen_Playing is
   Screen_Name : constant String := "Playing screen";

   procedure Run (State : in out Game.State_Type) is
      Termination : Manager.Termination_Type;

      task Server_Task is
         entry Update_Screen;
         entry Rotate;
         entry Move_Left;
         entry Move_Right;
         entry Move_Down;
         entry Remove_Completed_Line;
         entry Parental_Check;
         entry Quit;
      end Server_Task;

      task Screen_Refresh_Task;
      task Step_Task;
      task Line_Removing_Task;
      task Parental_Controller_Task;
      task Keyboard_Task;

      task body Server_Task is
      begin
         Logger.Debug (Screen_Name & ": Server Task started");
         Terminal.Erase_Screen;
         State.Show_Field;
         while not Termination.Is_Task_Terminated loop
            select
               accept Update_Screen do
                  Manager.Print_Changed_Screen (State);
               end Update_Screen;
            or
               accept Rotate do
                  Rotate_Piece (State);
               end Rotate;
            or
               accept Move_Left do
                  Move_Piece_Left (State);
               end Move_Left;
            or
               accept Move_Right do
                  Move_Piece_Right (State);
               end Move_Right;
            or
               accept Move_Down do
                  Move_Piece_Down (State, Termination);
               end Move_Down;
            or
               accept Remove_Completed_Line do
                  declare
                     Removed : Boolean;
                  begin
                     State.Remove_Completed_Line_From_Field (Removed);
                     if Removed then
                        State.Increment_Score;
                     end if;
                  end;
               end Remove_Completed_Line;
            or
               accept Parental_Check do
                  if Parental_Controller.Time_Exceeded then
                     State.Set_App_State (Game.State_Timeout);
                     Termination.Terminate_Task;
                  else
                     Parental_Controller.Register_Tick;
                  end if;
               end Parental_Check;
            or
               accept Quit do
                  Termination.Terminate_Task;
                  State.Reset_Game;
                  State.Set_App_State (Game.State_Intro);
               end Quit;
            end select;
         end loop;
         Logger.Debug (Screen_Name & ": Server Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in screen task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Termination.Terminate_Task;
      end Server_Task;

      task body Screen_Refresh_Task is
      begin
         Logger.Debug (Screen_Name & ": Screen Refresh Task started");
         while not Termination.Is_Task_Terminated loop
            Server_Task.Update_Screen;
            delay Config.Screen_Refresh_Interval;
         end loop;
         Logger.Debug (Screen_Name & ": Screen Refresh Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in Screen Refresh task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Server_Task.Quit;
      end Screen_Refresh_Task;

      task body Step_Task is
      begin
         Logger.Debug (Screen_Name & ": Step Task started");
         Logger.Debug ("Creating first piece");
         State.Set_Piece (Pieces.Create_Random_Piece);
         while not Termination.Is_Task_Terminated loop
            Server_Task.Move_Down;
            delay (case State.Get_Level is
               when 1 => Config.Step_Interval_1,
               when 2 => Config.Step_Interval_2,
               when 3 => Config.Step_Interval_3,
               when 4 => Config.Step_Interval_4
               );
         end loop;
         Logger.Debug (Screen_Name & ": Step Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in step task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Server_Task.Quit;
      end Step_Task;

      task body Line_Removing_Task is
      begin
         Logger.Debug (Screen_Name & ": Line Removing Task started");
         while not Termination.Is_Task_Terminated loop
            Server_Task.Remove_Completed_Line;
            delay Config.Line_Removing_Interval;
         end loop;
         Logger.Debug (Screen_Name & ": Line Removing Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in Line Removing task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Server_Task.Quit;
      end Line_Removing_Task;

      task body Parental_Controller_Task is
      begin
         Logger.Debug (Screen_Name & ": Parental Controller Task started");
         while not Termination.Is_Task_Terminated loop
            Server_Task.Parental_Check;
            --  10s
            for I in 1 .. Config.Parental_Check_Iterations loop
               exit when Termination.Is_Task_Terminated;
               delay Config.Parental_Check_Interval;
            end loop;
         end loop;
         Logger.Debug (Screen_Name & ": Parental Controller Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in Parental Controller task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Server_Task.Quit;
      end Parental_Controller_Task;

      task body Keyboard_Task is
         Key : Keyboard.Keyboard_Code;
      begin
         Logger.Debug (Screen_Name & ": Keyboard Task started");
         while not Termination.Is_Task_Terminated loop
            Key := Keyboard.Read_Keyboard;
            case Key is
               when Keyboard.Key_Left => Server_Task.Move_Left;
               when Keyboard.Key_Right => Server_Task.Move_Right;
               when Keyboard.Key_Up => Server_Task.Rotate;
               when Keyboard.Key_Down => Server_Task.Move_Down;
               when Keyboard.Key_Q => Server_Task.Quit;
               when others => null;
            end case;
         end loop;
         Logger.Debug (Screen_Name & ": Keyboard Task ended");
      exception
         when Exc : others =>
            Logger.Error (Screen_Name
               & ": Unhandled exception in keyboard task:"
               & Ada.Exceptions.Exception_Name (Exc)
               & ":"
               & Ada.Exceptions.Exception_Message (Exc));
            Server_Task.Quit;
      end Keyboard_Task;
   begin
      null;
   end Run;

   procedure Rotate_Piece (State : in out Game.State_Type)
   is
   begin
      if State.Get_Piece.Visible then
         declare
            Tmp : Canvases.Rectangle_Object := State.Get_Piece;
         begin
            Pieces.Rotate_Left (Tmp);
            if not Collider.Collision_Detected (Tmp, State.Get_Field) then
               State.Set_Piece (Tmp);
            end if;
         end;
      end if;
   end Rotate_Piece;

   procedure Move_Piece_Left (State : in out Game.State_Type)
   is
   begin
      if State.Get_Piece.Visible then
         declare
            Tmp : Canvases.Rectangle_Object := State.Get_Piece;
         begin
            Pieces.Move_Left (Tmp);
            if not Collider.Collision_Detected (Tmp, State.Get_Field) then
               State.Set_Piece (Tmp);
            end if;
         end;
      end if;
   end Move_Piece_Left;

   procedure Move_Piece_Right (State : in out Game.State_Type)
   is
   begin
      if State.Get_Piece.Visible then
         declare
            Tmp : Canvases.Rectangle_Object := State.Get_Piece;
         begin
            Pieces.Move_Right (Tmp);
            if not Collider.Collision_Detected (Tmp, State.Get_Field) then
               State.Set_Piece (Tmp);
            end if;
         end;
      end if;
   end Move_Piece_Right;

   procedure Move_Piece_Down (
      State : in out Game.State_Type;
      Termination : in out Manager.Termination_Type)
   is
   begin
      if State.Get_Piece.Visible then
         declare
            Tmp : Canvases.Rectangle_Object := State.Get_Piece;
         begin
            if Collider.Collision_Detected (Tmp, State.Get_Field) then
               Logger.Debug ("Detected collision before moving down");
               State.Set_App_State (Game.State_Game_Over);
               return;
            end if;
            Pieces.Move_Down (Tmp);
            if Collider.Collision_Detected (Tmp, State.Get_Field) then
               Logger.Debug ("Detected collision");
               if not Pieces.Is_Over_Field (Tmp, State.Get_Field) then
                  Logger.Debug ("Piece is over the field");
                  State.Set_App_State (Game.State_Game_Over);
                  Termination.Terminate_Task;
                  return;
               end if;
               State.Paste_Piece_Field;
               Tmp := Pieces.Create_Random_Piece;
               if Collider.Collision_Detected (Tmp, State.Get_Field) then
                  Logger.Debug ("Detected collision of new block");
                  State.Set_App_State (Game.State_Game_Over);
               end if;
               State.Set_Piece (Tmp);
            else
               State.Set_Piece (Tmp);
            end if;
         end;
      end if;
   end Move_Piece_Down;

   procedure Paste_Screen (
      Canv : in out Canvases.Canvas_Type;
      Piece : Canvases.Rectangle_Object;
      Field : Canvases.Rectangle_Object;
      Score : Integer)
   is
      Trimmed_Piece : Canvases.Rectangle_Object := Piece;
   begin
      Paste_Game_Background (Canv, Field);
      Canvases.Hide_Cells_Above (Trimmed_Piece,
         Config.Playing_Field_Size.Height);
      Canvases.Paste (Canv, Trimmed_Piece, Config.Playing_Field_Pos);
      Canvases.Paste (Canv, Field, Config.Playing_Field_Pos);
      Paste_Title (Canv);
      Paste_Score (Canv, Score);
   end Paste_Screen;

   procedure Paste_Game_Background (
      Canv : in out Canvases.Canvas_Type;
      Field : Canvases.Rectangle_Object)
   is
   begin
      --  Background of the entire page except the playing field
      for X in Canv'Range (1) loop
         for Y in Canv'Range (2) loop
            Canv (X, Y).Visible := True;
            Canv (X, Y).Material := False;
            if Canvases.Is_In (X, Y, Field, Config.Playing_Field_Pos)
            then
               Canv (X, Y).Background_Color := Config.Playing_Field_Color;
            else
               Canv (X, Y).Background_Color := Config.Screen_Color;
            end if;
         end loop;
      end loop;
   end Paste_Game_Background;

   procedure Paste_Title (Canv : in out Canvases.Canvas_Type)
   is
      Text_Obj_1 : constant Canvases.Rectangle_Object
         := Typography.Create_Text (
            Config.Game_Name_Part_1,
            Config.Playing_Title_Color);
      Pos_1 : constant Integer_Coordinates.Coordinates := (
         Config.Playing_Field_Pos.X
            + Config.Playing_Field_Size.Width
            + Config.Playing_Title_Offset,
         Config.Playing_Field_Pos.Y
            + Config.Playing_Field_Size.Height
            - Text_Obj_1.Height);
      Text_Obj_2 : constant Canvases.Rectangle_Object
         := Typography.Create_Text (
            Config.Game_Name_Part_2,
            Config.Playing_Title_Color);
      Pos_2 : constant Integer_Coordinates.Coordinates := (
         Config.Screen_Size.Width
            - Text_Obj_2.Width
            - Config.Playing_Title_Offset,
         Pos_1.Y
            - Text_Obj_1.Height);
   begin
      Canvases.Paste (Canv, Text_Obj_1, Pos_1);
      Canvases.Paste (Canv, Text_Obj_2, Pos_2);
   end Paste_Title;

   procedure Paste_Score (Canv : in out Canvases.Canvas_Type; Score : Integer)
   is
      Label_Obj : constant Canvases.Rectangle_Object
         := Typography.Create_Text ("P: ", Config.Playing_Score_Label_Color);
      Score_Text : constant String := Ada.Strings.Fixed.Trim (
         Score'Image, Ada.Strings.Left);
      Value_Obj : constant Canvases.Rectangle_Object
         := Typography.Create_Text (
            Score_Text,
            Config.Playing_Score_Value_Color);
      Pos : Integer_Coordinates.Coordinates := (
         Config.Playing_Field_Pos.X
         + Config.Playing_Field_Size.Width
         + Config.Playing_Score_Offset,
         Config.Playing_Field_Pos.Y);
   begin
      Canvases.Paste (Canv, Label_Obj, Pos);
      Pos.X := Pos.X + Label_Obj.Width + 1;
      Canvases.Paste (Canv, Value_Obj, Pos);
   end Paste_Score;

   function Get_Legend return Shortcut_Legend.Legend_Item_Vector.Vector
   is
      Legend : Shortcut_Legend.Legend_Item_Vector.Vector;
   begin
      Shortcut_Legend.Add_Item (Legend, "Q", "stop the game");
      Shortcut_Legend.Add_Item (Legend, "left right", "move");
      Shortcut_Legend.Add_Item (Legend, "up", "rotate");
      Shortcut_Legend.Add_Item (Legend, "bottom", "speed up");
      return Legend;
   end Get_Legend;

end Screen_Playing;