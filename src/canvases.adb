with Config;

package body Canvases is
   procedure Clear (Canv : in out Canvas_Type)
   is
      C : Cell;
   begin
      for X in Canv'Range (1) loop
         for Y in Canv'Range (2) loop
            Canv (X, Y) := C;
         end loop;
      end loop;
   end Clear;

   procedure Set_Background (
      Canv : in out Canvases.Canvas_Type;
      Background_Color : Terminal.Color)
   is
   begin
      for X in Canv'Range (1) loop
         for Y in Canv'Range (2) loop
            Canv (X, Y).Background_Color := Background_Color;
         end loop;
      end loop;
   end Set_Background;

   function Is_In (X, Y : Integer;
      Obj : Rectangle_Object;
      Obj_Offset : Integer_Coordinates.Coordinates)
      return Boolean
   is
      Left : constant Integer
         := Obj.X + Obj.Canvas'First (1) - 1 + Obj_Offset.X;
      Right : constant Integer
         := Obj.X + Obj.Canvas'Last (1) - 1 + Obj_Offset.X;
      Bottom : constant Integer
         := Obj.Y + Obj.Canvas'First (2) - 1 + Obj_Offset.Y;
      Top : constant Integer
         := Obj.Y + Obj.Canvas'Last (2) - 1 + Obj_Offset.Y;
   begin
      if X >= Left and then X <= Right
         and then Y >= Bottom and then Y <= Top
      then
         return True;
      else
         return False;
      end if;
   end Is_In;

   procedure Hide_Cells_Above (Obj : in out Rectangle_Object; Ref_Y : Integer)
   is
   begin
      for Y in Obj.Canvas'Range (2) loop
         declare
            Abs_Y : constant Integer := Obj.Y + Y - 1;
         begin
            if Abs_Y > Ref_Y then
               for X in Obj.Canvas'Range (1) loop
                  Obj.Canvas (X, Y).Visible := False;
               end loop;
            end if;
         end;
      end loop;
   end Hide_Cells_Above;

   procedure Paste (
      Tgt : in out Canvas_Type;
      Src : Rectangle_Object;
      Offset : Integer_Coordinates.Coordinates)
   is
   begin
      if Src.Visible then
         for X in Src.Canvas'Range (1) loop
            for Y in Src.Canvas'Range (2) loop
               if Src.Canvas (X, Y).Visible then
                  declare
                     Co : constant Integer_Coordinates.Coordinates := (
                        Offset.X + Src.X + X - 1,
                        Offset.Y + Src.Y + Y - 1);
                  begin
                     if Co.X in Tgt'Range (1) and then Co.Y in Tgt'Range (2)
                     then
                        Tgt (Co.X, Co.Y) := Src.Canvas (X, Y);
                     end if;
                  end;
               end if;
            end loop;
         end loop;
      end if;
   end Paste;

   procedure Paste (Tgt : in out Rectangle_Object; Src : Rectangle_Object)
   is
      Offset : constant Integer_Coordinates.Coordinates := (
         Src.X - Tgt.X,
         Src.Y - Tgt.Y);
   begin
      for X in Src.Canvas'Range (1) loop
         for Y in Src.Canvas'Range (2) loop
            if Src.Canvas (X, Y).Visible then
               declare
                  Tgt_Pos : constant Integer_Coordinates.Coordinates := (
                     Offset.X + X,
                     Offset.Y + Y);
               begin
                  if Tgt_Pos.X >= Tgt.Canvas'First (1)
                     and then Tgt_Pos.X <= Tgt.Canvas'Last (1)
                     and then Tgt_Pos.Y >= Tgt.Canvas'First (2)
                     and then Tgt_Pos.Y <= Tgt.Canvas'Last (2)
                  then
                     Tgt.Canvas (Tgt_Pos.X, Tgt_Pos.Y) := Src.Canvas (X, Y);
                  end if;
               end;
            end if;
         end loop;
      end loop;
   end Paste;

   function Equals (C1, C2 : Canvas_Type) return Boolean
   is
      use Terminal;
   begin
      if C1'First (1) /= C2'First (1) or else C1'Last (1) /= C2'Last (1) then
         return False;
      end if;
      if C1'First (2) /= C2'First (2) or else C1'Last (2) /= C2'Last (2) then
         return False;
      end if;
      for X in C1'Range (1) loop
         for Y in C1'Range (2) loop
            if C1 (X, Y).Text /= C2 (X, Y).Text then
               return False;
            end if;
            if C1 (X, Y).Color /= C2 (X, Y).Color then
               return False;
            end if;
            if C1 (X, Y).Background_Color /= C2 (X, Y).Background_Color then
               return False;
            end if;
         end loop;
      end loop;
      return True;
   end Equals;

   function Is_Line_Completed (Canv : Canvas_Type; Y : Positive)
      return Boolean
   is
   begin
      for X in Canv'Range (1) loop
         if not Canv (X, Y).Material then
            return False;
         end if;
      end loop;
      return True;
   end Is_Line_Completed;

   procedure Clear_Line (Canv : in out Canvas_Type; Y : Positive)
   is
      C : Cell;
   begin
      for X in Canv'Range (1) loop
         Canv (X, Y) := C;
      end loop;
   end Clear_Line;

   procedure Move_Range_Down (Canv : in out Canvas_Type;
      From_Line : Positive;
      To_Line : Positive)
   is
      Normalized_From_Line : constant Positive :=
         (if From_Line > Canv'First (2)
            then From_Line
            else From_Line + 1);
   begin
      for Y in Normalized_From_Line .. To_Line loop
         for X in Canv'Range (1) loop
            Canv (X, Y - 1) := Canv (X, Y);
         end loop;
      end loop;
      Clear_Line (Canv, To_Line);
   end Move_Range_Down;

   function Create_Filled_Rectangle (
      Width, Height : Positive;
      Fill_Color : Terminal.Color)
      return Rectangle_Object
   is
      Obj : Rectangle_Object (Width, Height);
      C : Cell;
   begin
      Obj.Visible := True;
      C.Visible := True;
      C.Background_Color := Fill_Color;
      for Y in 1 .. Height loop
         for X in 1 .. Width loop
            Obj.Canvas (X, Y) := C;
         end loop;
      end loop;
      return Obj;
   end Create_Filled_Rectangle;

   procedure Print (
      Canv : Canvas_Type;
      Legend : Shortcut_Legend.Legend_Item_Vector.Vector)
   is
      C : Cell;
      Legend_Left_Margin : constant String := "  ";
      Legend_Item_Separator : constant String := "    ";
      Legend_Key_Desc_Separator : constant String := " ";
      Written_Legend_Characters : Natural := 0;
   begin
      Terminal.Set_Cursor_Home;
      for Y in reverse Canv'First (2) .. Canv'Last (2) loop
         for X in Canv'First (1) .. Canv'Last (1) loop
            C := Canv (X, Y);
            Print_Cell (C);
         end loop;
         Terminal.New_Line;
      end loop;

      Terminal.Set_Background_Color (Config.Legend_Background_Color);
      Terminal.Put (Legend_Left_Margin);
      Written_Legend_Characters := Written_Legend_Characters
         + Legend_Left_Margin'Length;
      for I in Legend.First_Index .. Legend.Last_Index loop
         if I /= Legend.First_Index then
            Terminal.Put (Legend_Item_Separator);
            Written_Legend_Characters := Written_Legend_Characters
               + Legend_Item_Separator'Length;
         end if;
         Terminal.Set_Text_Color (Config.Legend_Key_Color);
         Terminal.Set_Background_Color (Config.Legend_Background_Color);
         Terminal.Put (Legend (I).Key);
         Written_Legend_Characters := Written_Legend_Characters
               + Legend (I).Key'Length;
         Terminal.Set_Text_Color (Config.Legend_Description_Color);
         Terminal.Set_Background_Color (Config.Legend_Background_Color);
         Terminal.Put (Legend_Key_Desc_Separator & Legend (I).Description);
         Written_Legend_Characters := Written_Legend_Characters
               + Legend_Key_Desc_Separator'Length
               + Legend (I).Description'Length;
      end loop;
      while Written_Legend_Characters <= Config.Screen_Size.Width * 2 loop
         Terminal.Put (" ");
         Written_Legend_Characters := Written_Legend_Characters + 1;
      end loop;
      Terminal.New_Line;
   end Print;

   procedure Print_Cell (C : Cell) is
      use Terminal;
   begin
      if C.Color /= Terminal.Default then
         Terminal.Set_Text_Color (C.Color);
      end if;
      if C.Background_Color /= Terminal.Default then
         Terminal.Set_Background_Color (C.Background_Color);
      else
         Terminal.Set_Background_Color (Terminal.Black);
      end if;
      Terminal.Put (C.Text);
      Terminal.Put (C.Text);
   end Print_Cell;

end Canvases;