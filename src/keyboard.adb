with Ada.Text_IO;
with Logger;

package body Keyboard is
   function Read_Keyboard return Keyboard_Code
   is
      Ch : Character;
      Available : Boolean;
      Result : Keyboard_Code := None;
      package TIO renames Ada.Text_IO;
   begin
      TIO.Get_Immediate (Ch, Available);
      if Available then
         if Ch = Character'Val (8#33#) then
            TIO.Get_Immediate (Ch, Available);
            if Available then
               if Ch = '[' then
                  TIO.Get_Immediate (Ch, Available);
                  if Available then
                     case Ch is
                        when 'A' => Result := Key_Up;
                        when 'B' => Result := Key_Down;
                        when 'C' => Result := Key_Right;
                        when 'D' => Result := Key_Left;
                        when others =>
                           Logger.Debug ("Unknown key: " & Ch);
                     end case;
                  end if;
               else
                  Logger.Debug ("Unknown key: " & Ch);
               end if;
            end if;
         elsif Ch = 'q' or else Ch = 'Q' then
            Result := Key_Q;
         elsif Ch = ASCII.LF then
            Result := Key_Enter;
         else
            Logger.Debug ("Unknown key: " & Ch);
         end if;
      end if;

      return Result;
   end Read_Keyboard;
end Keyboard;
