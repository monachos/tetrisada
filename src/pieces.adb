with Ada.Numerics.Discrete_Random;
with Config;
with Integer_Coordinates;
with Logger;
with Terminal;

package body Pieces is
   function Get_Initial_Piece_Coordinates
      return Integer_Coordinates.Coordinates
   is
      Co : constant Integer_Coordinates.Coordinates := (
         Config.Playing_Field_Size.Width / 2,
         Config.Playing_Field_Size.Height + 1);
   begin
      return Co;
   end Get_Initial_Piece_Coordinates;

   type Piece_Setter_Access is access procedure
      (Piece : in out Canvases.Rectangle_Object);

   Piece_Setters : constant array (Positive range <>)
      of Piece_Setter_Access := (
      Set_I'Access,
      Set_L1'Access,
      Set_L2'Access,
      Set_S1'Access,
      Set_S2'Access,
      Set_T'Access,
      Set_O'Access
   );
   subtype Piece_Index_Type is Positive range Piece_Setters'Range;
   package Random_Piece_Index is
      new Ada.Numerics.Discrete_Random (Piece_Index_Type);
   Piece_Index_Generator : Random_Piece_Index.Generator;

   function Get_Random_Piece_Setter return Piece_Setter_Access
   is
      Index : constant Piece_Index_Type
         := Random_Piece_Index.Random (Piece_Index_Generator);
   begin
      return Piece_Setters (Index);
   end Get_Random_Piece_Setter;

   Piece_Colors : constant array (Positive range <>) of Terminal.Color := (
      Terminal.Red,
      Terminal.Green,
      Terminal.Blue,
      Terminal.Magenta,
      Terminal.Cyan,
      Terminal.Yellow
   );
   subtype Color_Index_Type is Positive range Piece_Colors'Range;
   package Random_Color_Index is
      new Ada.Numerics.Discrete_Random (Color_Index_Type);
   Color_Index_Generator : Random_Color_Index.Generator;

   function Get_Random_Color return Terminal.Color
   is
      Index : constant Color_Index_Type
         := Random_Color_Index.Random (Color_Index_Generator);
   begin
      return Piece_Colors (Index);
   end Get_Random_Color;

   function Create_Random_Piece return Canvases.Rectangle_Object
   is
      Piece : Canvases.Rectangle_Object (
         Piece_Canvas_Size.Width,
         Piece_Canvas_Size.Height);
   begin
      Get_Random_Piece_Setter.all (Piece);
      return Piece;
   end Create_Random_Piece;

   procedure Clear (Piece : in out Canvases.Rectangle_Object)
   is
      C : Canvases.Canvas_Type (
         Piece.Canvas'Range (1),
         Piece.Canvas'Range (2));
   begin
      Piece.X := 1;
      Piece.Y := 1;
      Piece.Canvas := C;
   end Clear;

   procedure Set_I (Piece : in out Canvases.Rectangle_Object)
   is
      C : Canvases.Cell;
      Co : constant Integer_Coordinates.Coordinates
         := Get_Initial_Piece_Coordinates;
   begin
      C.Visible := True;
      C.Material := True;
      C.Color := Terminal.White;
      C.Background_Color := Get_Random_Color;

      Clear (Piece);

      Piece.X := Co.X;
      Piece.Y := Co.Y;
      Piece.Canvas (1, 2) := C;
      Piece.Canvas (2, 2) := C;
      Piece.Canvas (3, 2) := C;
      Piece.Canvas (4, 2) := C;
      Piece.Visible := True;
   end Set_I;

   procedure Set_L1 (Piece : in out Canvases.Rectangle_Object)
   is
      C : Canvases.Cell;
      Co : constant Integer_Coordinates.Coordinates
         := Get_Initial_Piece_Coordinates;
   begin
      C.Visible := True;
      C.Material := True;
      C.Color := Terminal.White;
      C.Background_Color := Get_Random_Color;

      Clear (Piece);

      Piece.X := Co.X;
      Piece.Y := Co.Y;
      Piece.Canvas (1, 2) := C;
      Piece.Canvas (2, 2) := C;
      Piece.Canvas (3, 2) := C;
      Piece.Canvas (1, 3) := C;
      Piece.Visible := True;
   end Set_L1;

   procedure Set_L2 (Piece : in out Canvases.Rectangle_Object)
   is
      C : Canvases.Cell;
      Co : constant Integer_Coordinates.Coordinates
         := Get_Initial_Piece_Coordinates;
   begin
      C.Visible := True;
      C.Material := True;
      C.Color := Terminal.White;
      C.Background_Color := Get_Random_Color;

      Clear (Piece);

      Piece.X := Co.X;
      Piece.Y := Co.Y;
      Piece.Canvas (1, 2) := C;
      Piece.Canvas (2, 2) := C;
      Piece.Canvas (3, 2) := C;
      Piece.Canvas (3, 3) := C;
      Piece.Visible := True;
   end Set_L2;

   procedure Set_S1 (Piece : in out Canvases.Rectangle_Object)
   is
      C : Canvases.Cell;
      Co : constant Integer_Coordinates.Coordinates
         := Get_Initial_Piece_Coordinates;
   begin
      C.Visible := True;
      C.Material := True;
      C.Color := Terminal.White;
      C.Background_Color := Get_Random_Color;

      Clear (Piece);

      Piece.X := Co.X;
      Piece.Y := Co.Y;
      Piece.Canvas (1, 2) := C;
      Piece.Canvas (2, 2) := C;
      Piece.Canvas (2, 3) := C;
      Piece.Canvas (3, 3) := C;
      Piece.Visible := True;
   end Set_S1;

   procedure Set_S2 (Piece : in out Canvases.Rectangle_Object)
   is
      C : Canvases.Cell;
      Co : constant Integer_Coordinates.Coordinates
         := Get_Initial_Piece_Coordinates;
   begin
      C.Visible := True;
      C.Material := True;
      C.Color := Terminal.White;
      C.Background_Color := Get_Random_Color;

      Clear (Piece);

      Piece.X := Co.X;
      Piece.Y := Co.Y;
      Piece.Canvas (1, 3) := C;
      Piece.Canvas (2, 3) := C;
      Piece.Canvas (2, 2) := C;
      Piece.Canvas (3, 2) := C;
      Piece.Visible := True;
   end Set_S2;

   procedure Set_T (Piece : in out Canvases.Rectangle_Object)
   is
      C : Canvases.Cell;
      Co : constant Integer_Coordinates.Coordinates
         := Get_Initial_Piece_Coordinates;
   begin
      C.Visible := True;
      C.Material := True;
      C.Color := Terminal.White;
      C.Background_Color := Get_Random_Color;

      Clear (Piece);

      Piece.X := Co.X;
      Piece.Y := Co.Y;
      Piece.Canvas (1, 2) := C;
      Piece.Canvas (2, 2) := C;
      Piece.Canvas (3, 2) := C;
      Piece.Canvas (2, 3) := C;
      Piece.Visible := True;
   end Set_T;

   procedure Set_O (Piece : in out Canvases.Rectangle_Object)
   is
      C : Canvases.Cell;
      Co : constant Integer_Coordinates.Coordinates
         := Get_Initial_Piece_Coordinates;
   begin
      C.Visible := True;
      C.Material := True;
      C.Color := Terminal.White;
      C.Background_Color := Get_Random_Color;

      Clear (Piece);

      Piece.X := Co.X;
      Piece.Y := Co.Y;
      Piece.Canvas (2, 2) := C;
      Piece.Canvas (2, 3) := C;
      Piece.Canvas (3, 2) := C;
      Piece.Canvas (3, 3) := C;
      Piece.Visible := True;
   end Set_O;

   procedure Move_Left (Piece : in out Canvases.Rectangle_Object)
   is
   begin
      Piece.X := Piece.X - 1;
   end Move_Left;

   procedure Move_Right (Piece : in out Canvases.Rectangle_Object)
   is
   begin
      Piece.X := Piece.X + 1;
   end Move_Right;

   procedure Move_Down (Piece : in out Canvases.Rectangle_Object)
   is
   begin
      Piece.Y := Piece.Y - 1;
   end Move_Down;

   procedure Rotate_Left (Piece : in out Canvases.Rectangle_Object)
   is
      Original : constant Canvases.Rectangle_Object := Piece;
   begin
      for Y in Original.Canvas'Range (2) loop
         for X in Original.Canvas'Range (1) loop
            Piece.Canvas (Original.Canvas'Last (2) + 1 - Y, X)
               := Original.Canvas (X, Y);
         end loop;
      end loop;
   end Rotate_Left;

   function Remove_Completed_Line (Canv : in out Canvases.Canvas_Type)
      return Boolean
   is
   begin
      for Y in Canv'Range (2) loop
         if Canvases.Is_Line_Completed (Canv, Y) then
            Logger.Debug ("Removing line" & Y'Image);
            Canvases.Clear_Line (Canv, Y);
            Canvases.Move_Range_Down (Canv, Y + 1, Canv'Last (2));
            return True;
         end if;
      end loop;
      return False;
   end Remove_Completed_Line;

   function Is_Over_Field (
      Piece : Canvases.Rectangle_Object;
      Field : Canvases.Rectangle_Object)
         return Boolean
   is
      Max_Y : constant Integer := Field.Y + Field.Canvas'Last (2) - 1;
   begin
      for Y in Piece.Canvas'Range (2) loop
         for X in Piece.Canvas'Range (1) loop
            if Piece.Canvas (X, Y).Material then
               declare
                  Piece_Y : constant Integer := Piece.Y + Y - 1;
               begin
                  if Piece_Y > Max_Y then
                     return False;
                  end if;
               end;
            end if;
         end loop;
      end loop;
      return True;
   end Is_Over_Field;
end Pieces;