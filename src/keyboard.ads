package Keyboard is
   type Keyboard_Code is (
      None,
      Key_Left,
      Key_Right,
      Key_Up,
      Key_Down,
      Key_Enter,
      Key_Q
      );

   function Read_Keyboard return Keyboard_Code;
end Keyboard;
