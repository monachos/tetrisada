with Ada.Strings.Fixed;
with Ada.Text_IO;
with Config;
with Logger;

package body Parental_Controller is
   package TIO renames Ada.Text_IO;

   procedure Init_Tick_Counter
   is
      File_Path : constant String := Config.Get_Log_File_Path;
      Log_File : TIO.File_Type;
   begin
      TIO.Open (
         File => Log_File,
         Mode => TIO.In_File,
         Name => File_Path);
      while not TIO.End_Of_File (Log_File) loop
         declare
            Line : constant String :=  TIO.Get_Line (Log_File);
         begin
            if Ada.Strings.Fixed.Index (Line, Tick_Marker_Name) > 0 then
               Tick_Counter := Tick_Counter + 1;
            end if;
         end;
      end loop;
      TIO.Close (Log_File);
   exception
      when TIO.Name_Error => null;
   end Init_Tick_Counter;

   function Time_Exceeded return Boolean
   is
   begin
      return Tick_Counter > 6 * Config.Limit_Of_Minutes_Per_Day;
   end Time_Exceeded;

   procedure Register_Tick
   is
   begin
      Tick_Counter := Tick_Counter + 1;
      Logger.Debug (Tick_Marker_Name & Tick_Counter'Image);
   end Register_Tick;

end Parental_Controller;