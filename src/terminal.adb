with Ada.Text_IO;

package body Terminal is
   package TIO renames Ada.Text_IO;

   procedure Set_Text_Color (C : Color) is
   begin
      Terminal.Set_Display_Attribute (Terminal.Get_Foreground_Color_Code (C));
   end Set_Text_Color;

   procedure Set_Background_Color (C : Color) is
   begin
      if C /= Default and then C /= Transparent
      then
         Terminal.Set_Display_Attribute (
            Terminal.Get_Background_Color_Code (C));
      end if;
   end Set_Background_Color;

   function Get_Foreground_Color_Code (C : Color)
      return String is
   begin
      case C is
         when Black =>
            return Attribute_Reset & ";" & Foreground_Black;
         when DarkGray =>
            return Attribute_Bright & ";" & Foreground_Black;
         when Red =>
            return Attribute_Reset & ";" & Foreground_Red;
         when LightRed =>
            return Attribute_Bright & ";" & Foreground_Red;
         when Green =>
            return Attribute_Reset & ";" & Foreground_Green;
         when LightGreen =>
            return Attribute_Bright & ";" & Foreground_Green;
         when Yellow =>
            return Attribute_Reset & ";" & Foreground_Yellow;
         when LightYellow =>
            return Attribute_Bright & ";" & Foreground_Yellow;
         when Blue =>
            return Attribute_Reset & ";" & Foreground_Blue;
         when LightBlue =>
            return Attribute_Bright & ";" & Foreground_Blue;
         when Magenta =>
            return Attribute_Reset & ";" & Foreground_Magenta;
         when LightMagenta =>
            return Attribute_Bright & ";" & Foreground_Magenta;
         when Cyan =>
            return Attribute_Reset & ";" & Foreground_Cyan;
         when LightCyan =>
            return Attribute_Bright & ";" & Foreground_Cyan;
         when LightGray =>
            return Attribute_Reset & ";" & Foreground_White;
         when White =>
            return Attribute_Bright & ";" & Foreground_White;
         when others =>
            raise Invalid_Color_Exception;
      end case;
   end Get_Foreground_Color_Code;

   function Get_Background_Color_Code (C : Color)
      return String is
   begin
      case C is
         when Black | DarkGray => return Background_Black;
         when Red | LightRed => return Background_Red;
         when Green | LightGreen => return Background_Green;
         when Yellow | LightYellow  => return Background_Yellow;
         when Blue | LightBlue => return Background_Blue;
         when Magenta | LightMagenta => return Background_Magenta;
         when Cyan | LightCyan => return Background_Cyan;
         when LightGray | White => return Background_White;
         when others => raise Invalid_Color_Exception;
      end case;
   end Get_Background_Color_Code;

   procedure Set_Display_Attribute (Attr : String) is
   begin
      TIO.Put (Terminal.Escape & "[" & Attr & "m");
   end Set_Display_Attribute;

   procedure Set_Display_Attributes (
      Attr1 : String;
      Attr2 : String) is
   begin
      TIO.Put (Terminal.Escape & "["
         & Attr1 & ";" & Attr2 & "m");
   end Set_Display_Attributes;

   procedure Set_Display_Attributes (
      Attr1 : String;
      Attr2 : String;
      Attr3 : String) is
   begin
      TIO.Put (Terminal.Escape & "["
         & Attr1 & ";" & Attr2 & ";" & Attr3 & "m");
   end Set_Display_Attributes;

   procedure Set_Cursor_Home is
   begin
      TIO.Put (Terminal.Escape & "[H");
   end Set_Cursor_Home;

   procedure Erase_Screen is
   begin
      TIO.Put (Terminal.Escape & "[2J");
   end Erase_Screen;

   procedure New_Line is
   begin
      TIO.New_Line;
   end New_Line;

   procedure Put (Ch : Character) is
   begin
      TIO.Put (Ch);
   end Put;

   procedure Put (Text : String) is
   begin
      TIO.Put (Text);
   end Put;
end Terminal;
