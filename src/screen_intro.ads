with Canvases;
with Game;
with Shortcut_Legend;

package Screen_Intro is
   procedure Run (State : in out Game.State_Type);

   procedure Paste_Screen (
      Canv : in out Canvases.Canvas_Type;
      Level : Game.Level_Type);

   function Get_Legend return Shortcut_Legend.Legend_Item_Vector.Vector;

private
   procedure Paste_Title (Canv : in out Canvases.Canvas_Type);
   procedure Paste_Levels (
      Canv : in out Canvases.Canvas_Type;
      Level : Game.Level_Type);
end Screen_Intro;