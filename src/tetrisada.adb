with Config;
with Logger;
with Manager;
with Parental_Controller;

procedure Tetrisada is
   Log_Path : constant String := Config.Get_Log_File_Path;
begin
   Config.Create_Profile_Directory_Of_Does_Not_Exist;
   Parental_Controller.Init_Tick_Counter;
   Logger.Init_Logger_File (Log_Path);
   Manager.Run;
   Logger.Info ("End");
end Tetrisada;
